/*
 * Copyright (c) 2014, Oracle America, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 *  * Neither the name of Oracle nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package ar.com.kfgodel.algos;

import ar.com.kfgodel.algos.impl.*;
import cern.colt.list.IntArrayList;
import gnu.trove.list.array.TIntArrayList;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@State(Scope.Benchmark)
public class PrimitiveCollectionsBenchmark {

  private InsertionSortIntArraySorter arraySorter = InsertionSortIntArraySorter.create();
  private InsertionSortIntegerListSorter listSorter = InsertionSortIntegerListSorter.create();
  private InsertionSortConfigurableOrderIntegerListSorter ascListSorter = InsertionSortConfigurableOrderIntegerListSorter.create((a, b) -> a <= b);
  private InsertionSortConfigurableOrderIntegerListSorter descListSorter = InsertionSortConfigurableOrderIntegerListSorter.create((a, b) -> a >= b);

  private InsertionSortTroveIntegerListSorter troveListSorter = InsertionSortTroveIntegerListSorter.create();
  private InsertionSortConfigurableTroveOrderIntegerListSorter ascTroveListSorter = InsertionSortConfigurableTroveOrderIntegerListSorter.create((a, b) -> a <= b);
  private InsertionSortConfigurableTroveOrderIntegerListSorter descTroveListSorter = InsertionSortConfigurableTroveOrderIntegerListSorter.create((a, b) -> a >= b);

  private InsertionSortColtIntegerListSorter coltListSorter = InsertionSortColtIntegerListSorter.create();

  @Benchmark
  public void noOpZero() {
    // This is a reference test so we know what the best case is on this machine
  }

  @Benchmark
  public void insertionSortIntArraySorter() {
    arraySorter.sort(createTestArray());
  }

  @Benchmark
  public void insertionSortIntegerListSorter() {
    List<Integer> elements = Arrays.stream(createTestArray())
      .mapToObj(Integer::valueOf)
      .collect(Collectors.toList());
    listSorter.sort(elements);
  }

  @Benchmark
  public void insertionSortConfigurableAscIntegerListSorter() {
    List<Integer> elements = Arrays.stream(createTestArray())
      .mapToObj(Integer::valueOf)
      .collect(Collectors.toList());
    ascListSorter.sort(elements);
  }

  @Benchmark
  public void insertionSortConfigurableDescIntegerListSorter() {
    List<Integer> elements = Arrays.stream(createTestArray())
      .mapToObj(Integer::valueOf)
      .collect(Collectors.toList());
    descListSorter.sort(elements);
  }

  @Benchmark
  public void insertionSortTroveIntegerListSorter() {
    troveListSorter.sort(TIntArrayList.wrap(createTestArray()));
  }

  @Benchmark
  public void insertionSortAscTroveIntegerListSorter() {
    ascTroveListSorter.sort(TIntArrayList.wrap(createTestArray()));
  }

  @Benchmark
  public void insertionSortDescTroveIntegerListSorter() {
    descTroveListSorter.sort(TIntArrayList.wrap(createTestArray()));
  }

  @Benchmark
  public void insertionSortColtIntegerListSorter() {
    coltListSorter.sort(new IntArrayList(createTestArray()));
  }

  private int[] createTestArray() {
    return new int[]{5, 2, 4, 6, 1, 3};
  }


  public static void main(String[] args) throws RunnerException {
    Options opt = new OptionsBuilder()
      .include(PrimitiveCollectionsBenchmark.class.getSimpleName())
      .warmupIterations(5)
      .measurementIterations(5)
      .timeUnit(TimeUnit.MICROSECONDS)
      .forks(1)
      .build();
    new Runner(opt).run();
  }
}


//  AlgoBenchmark.insertionSortAscTroveIntegerListSorter          thrpt   20    45.227 ± 0.173  ops/us
//  AlgoBenchmark.insertionSortColtIntegerListSorter              thrpt   20    38.827 ± 0.175  ops/us
//  AlgoBenchmark.insertionSortConfigurableAscIntegerListSorter   thrpt   20    11.623 ± 0.041  ops/us
//  AlgoBenchmark.insertionSortConfigurableDescIntegerListSorter  thrpt   20    13.907 ± 0.121  ops/us
//  AlgoBenchmark.insertionSortDescTroveIntegerListSorter         thrpt   20    56.925 ± 0.212  ops/us
//  AlgoBenchmark.insertionSortIntArraySorter                     thrpt   20    50.709 ± 0.165  ops/us
//  AlgoBenchmark.insertionSortIntegerListSorter                  thrpt   20    18.571 ± 0.359  ops/us
//  AlgoBenchmark.insertionSortTroveIntegerListSorter             thrpt   20    42.834 ± 0.120  ops/us
//  AlgoBenchmark.noOpZero                                        thrpt   20  3801.579 ± 7.438  ops/us