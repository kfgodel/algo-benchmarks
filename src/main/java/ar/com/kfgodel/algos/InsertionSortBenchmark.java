/*
 * Copyright (c) 2014, Oracle America, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 *  * Neither the name of Oracle nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package ar.com.kfgodel.algos;

import ar.com.kfgodel.algos.impl.InsertionSortConfigurableTroveOrderIntegerListSorter;
import ar.com.kfgodel.algos.impl.InsertionSortIntArraySorter;
import ar.com.kfgodel.algos.impl.InsertionSortIntegerListSorter;
import ar.com.kfgodel.algos.impl.InsertionSortTroveIntegerListSorter;
import gnu.trove.list.array.TIntArrayList;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@State(Scope.Benchmark)
public class InsertionSortBenchmark {

  @Param({"1", "20", "40", "60", "80", "100", "1000", "10000"})
  private int listSize;

  private InsertionSortIntArraySorter arraySorter = InsertionSortIntArraySorter.create();
  private InsertionSortIntegerListSorter listSorter = InsertionSortIntegerListSorter.create();
  private InsertionSortTroveIntegerListSorter troveListSorter = InsertionSortTroveIntegerListSorter.create();
  private InsertionSortConfigurableTroveOrderIntegerListSorter ascTroveListSorter = InsertionSortConfigurableTroveOrderIntegerListSorter.create((a, b) -> a <= b);
  private InsertionSortConfigurableTroveOrderIntegerListSorter descTroveListSorter = InsertionSortConfigurableTroveOrderIntegerListSorter.create((a, b) -> a >= b);

  @Benchmark
  public void noOpZero() {
    // This is a demo/sample template for building your JMH benchmarks. Edit as needed.
    // Put your benchmark code here.
  }

  @Benchmark
  public void insertionSortIntArraySorter() {
    arraySorter.sort(createTestArray());
  }

  @Benchmark
  public void insertionSortIntegerListSorter() {
    List<Integer> elements = Arrays.stream(createTestArray())
      .mapToObj(Integer::valueOf)
      .collect(Collectors.toList());
    listSorter.sort(elements);
  }

  @Benchmark
  public void insertionSortTroveIntegerListSorter() {
    troveListSorter.sort(TIntArrayList.wrap(createTestArray()));
  }

  @Benchmark
  public void insertionSortAscTroveIntegerListSorter() {
    ascTroveListSorter.sort(TIntArrayList.wrap(createTestArray()));
  }

  @Benchmark
  public void insertionSortDescTroveIntegerListSorter() {
    descTroveListSorter.sort(TIntArrayList.wrap(createTestArray()));
  }

  private int[] createTestArray() {
    Random random = new Random(234234);
    int[] elements = new int[listSize];
    for (int i = 0; i < listSize; i++) {
      elements[i] = random.nextInt();
    }
    return elements;
  }


  public static void main(String[] args) throws RunnerException {
    Options opt = new OptionsBuilder()
      .include(InsertionSortBenchmark.class.getSimpleName())
      .warmupIterations(5)
      .measurementIterations(5)
      .timeUnit(TimeUnit.MILLISECONDS)
      .forks(1)
      .build();
    new Runner(opt).run();
  }
}